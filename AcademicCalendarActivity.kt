package com.buyuk.sactinapp.ui.student.AcademicCalendar

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.buyuk.sactin.Api.APIClient
import com.buyuk.sactin.Api.APIInterface
import com.buyuk.sactinapp.R
import com.buyuk.sactinapp.SharedPrefManager
import com.events.calendar.views.EventsCalendar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AcademicCalendarActivity : AppCompatActivity(), EventsCalendar.Callback{

    lateinit var eventsCalendar:EventsCalendar
    var eventList: ArrayList<EventModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_academic_calendar)
        eventsCalendar=findViewById(R.id.eventsCalendar)

        val today = Calendar.getInstance()
        val end = Calendar.getInstance()
        end.add(Calendar.YEAR, 2)
        eventsCalendar.setSelectionMode(eventsCalendar.MULTIPLE_SELECTION)
            .setToday(today)
            .setMonthRange(today, end)
            .setWeekStartDay(Calendar.SUNDAY, false)
            .setIsBoldTextOnSelectionEnabled(true)
            .setCallback(this)
            .build()





        val dc = Calendar.getInstance()
        dc.add(Calendar.DAY_OF_MONTH, 2)

        getEvents()
    }

    fun getEvents(){


        val mIntent = intent
        val batchId = mIntent.getIntExtra("BATCH_ID", 0)
        val call  = APIClient.getInstance()!!.getClient(this)!!.create(APIInterface::class.java).getEvents(batchId)
        call?.enqueue(object : Callback<APIInterface.Model.GetEventResult> {
            override fun onResponse(
                call: Call<APIInterface.Model.GetEventResult>,
                response: Response<APIInterface.Model.GetEventResult>
            ) {

                eventList=response.body()!!.data

                for(data in eventList) {

                    val date = data.event_start_date
                    val c = Calendar.getInstance()
                    c.add(Calendar.DAY_OF_MONTH,date.toInt())
                    eventsCalendar.addEvent(c)
                    eventsCalendar.addEvent(c)


               // two addEvent functions are available in this sample,second function that take string value as parameter .
               //that is the below code .but not working

//                    val date2=data.event_start_date
//                    eventsCalendar.addEvent(date2)

                }


                if(response.body()!!.status) {

                }
                else
                {

                }

            }

            override fun onFailure(call: Call<APIInterface.Model.GetEventResult>, t: Throwable)
            {
                Toast.makeText(applicationContext, "Unable to Connect, Please Check your Internet Connection", Toast.LENGTH_SHORT)
                call.cancel()
            }
        })


    }

    override fun onDayLongPressed(selectedDate: Calendar?) {

    }

    override fun onDaySelected(selectedDate: Calendar?) {
        Toast.makeText(applicationContext,"holiday", Toast.LENGTH_SHORT).show()

    }

    override fun onMonthChanged(monthStartDate: Calendar?) {

    }

}