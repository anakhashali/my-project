package com.buyuk.sactinapp.ui.student.AcademicCalendar

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EventModel(
    @SerializedName("pk_int_school_events_id") val pk_int_school_events_id: Int,
    @SerializedName("vchr_school_event") val vchr_school_event: String,
    @SerializedName("vchr_school_event_desc") val vchr_school_event_desc: String?,
    @SerializedName("fk_int_batch_id") val fk_int_batch_id: Int,
    @SerializedName("event_start_date") val event_start_date: String,
    @SerializedName("event_end_date") val event_end_date: String,
    ) : Serializable